# sources every file with a .rc extension in this directory
for conf in $HOME/.config/zsh/*.rc; do
	source "${conf}"
done
