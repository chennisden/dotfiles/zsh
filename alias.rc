# Rust replacements for GNU utils

alias l="exa"
alias ls="exa"

# Editing dotfiles

## zsh

alias zsource="source ~/.config/zsh/.zshenv && source ~/.config/zsh/.zshrc" # sources from zshenv and zshrc

### env

alias zpath="$EDITOR ~/.config/zsh/path.env"
alias zvars="$EDITOR ~/.config/zsh/vars.env"

### rc

alias zopt="$EDITOR ~/.config/zsh/opt.rc"
alias zalias="$EDITOR ~/.config/zsh/alias.rc"
alias zprompt="$EDITOR ~/.config/zsh/prompt.rc"
alias zcolor="$EDITOR ~/.config/zsh/color.rc"

# Misc

clip() {
	# copies a file's contents to clipboard
	cat $1 | wl-copy
	# show what was copied for convenience 
	cat $1
}

# Git clone

ghub-clone(){
	# example:
	#		ghub-clone chennisden/dotfiles
	if [ -z "$1" ]; then
		echo "Must pass in an argument"
		echo "Usage: ghub-clone chennisden/dotfiles"
	else
		git clone git@github.com:$1.git
	fi
}

glab-clone(){
	# example:
	#		glab-clone chennisden/dotfiles/zsh
	if [ -z "$1" ]; then
		echo "Must pass in an argument"
		echo "Usage: glab-clone chennisden/dotfiles/zsh"
	else
		git clone git@gitlab.com:$1.git
	fi
}

mast-unit-clone() {
	if [ -z "$1" ]; then
		echo "Must pass in an argument"
		echo "Usage: mast-unit-clone CQV-Perspectives"
	else
		git clone git@gitlab.com:mathadvance/mast/units/$1.git
	fi
}

mast-draft-clone() {
	if [ -z "$1" ]; then
		echo "Must pass in an argument"
		echo "Usage: mast-draft-clone CQV-Perspectives"
	else
		git clone git@gitlab.com:chennisden/mast-drafts/$1.git
	fi
}
